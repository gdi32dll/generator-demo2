const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  mode: "development",
  entry: {
    main: './src/app/index.ts'
  },
  devtool: 'inline-source-map',
  target: 'node',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'publish'),
    publicPath: "/",
    libraryTarget: 'commonjs2'
  },
  node: {
    __filename: true
  },
  module: {
    rules: [
      {
        test: /\.ts$/i,
        use: 'ts-loader',
        exclude: /(node_modules)|(.test.ts$)|(.template.js$)/
      },
      {
        resourceQuery: /raw/,
        type: 'asset/source',
        exclude: /(node_modules)/
      }
    ],
    noParse: [require.resolve('typescript/lib/typescript.js')],
  },
  plugins: [
    new CleanWebpackPlugin()
  ],
  resolve: {
    extensions: ['.ts', '.js']
  }
}
