import {BaseRender} from './token-render';
import {TokenBase, TokenInvokedBase, TokenNumber, TokenString} from '../tokens/tokens';
import {RenderDependency} from './render-infrastructure';
import {TokenType} from '../tokens/token-types';

export class StringRenderMock extends BaseRender<TokenString> {
  render(token: TokenString): string {
    return `'value'`;
  }
}

export class NumberRenderMock extends BaseRender<TokenNumber> {
  render(token: TokenNumber) {
    return '10';
  }
}

export class RenderDependencyMock implements RenderDependency {

  append(tokenName: TokenType, render: BaseRender<TokenBase | TokenInvokedBase>) {
    this[tokenName] = render;
  }

  getRender(tokenName: TokenType): BaseRender<TokenBase | TokenInvokedBase> {
    const render = this[tokenName];
    if (!render) {
      throw new Error(`Can't find Render for ${tokenName || 'undefined'}`)
    }
    return render;
  }

  static createForObject() {
    const mock = new RenderDependencyMock();
    mock.append('string', new StringRenderMock());
    mock.append('number', new NumberRenderMock());
    return mock;
  }
}
