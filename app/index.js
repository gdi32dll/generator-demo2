const Generator = require('yeoman-generator');
const chalk = require('chalk');
const {MyGenerator} = require('../publish/main');


module.exports = class extends Generator {

  _appName = 'app1';
  _userName = 'Anon';
  _opts = null;

  constructor(args, opts) {
    // Calling the super constructor is important so our generator is correctly set up
    super(args, opts);

    this.argument('name', {
      type: String,
      required: true,
      description: 'Generator name'
    });
  }

  prompting() {
    const generator = new MyGenerator();
    this.log(chalk.red('Welcome to demo 2 generator!'));
    this.log(generator.getMessage(this._userName));
    this.log('Path: ' + generator.path());
    this.log(generator.code());
    // this.log(this.options);
  }
};
