import { parse, parseExpression } from "@babel/parser";
import generate from "@babel/generator";
import {
  BlockStatement,
  CallExpression,
  ExpressionStatement,
  FunctionDeclaration, Identifier,
  IfStatement,
  MemberExpression, StringLiteral
} from '@babel/types';
import {importFile} from './import-file';

const codeMyTest = `
function myTest(a) {
  if (typeof a === 'string') {
    console.log(a);
  } else {
    console.log('"a" is not a string');
  }
}`;

test('parse babel code from codeMyTest', () => {
  const ast = parse(codeMyTest);
  expect(ast.program.body).not.toBeUndefined();
  expect(ast.program.body.length).toBe(1);

  const funcAst = ast.program.body[0] as FunctionDeclaration;
  expect(funcAst.type).toBe('FunctionDeclaration');
  expect(funcAst.id.name).toBe('myTest');
  expect(funcAst.body.body.length).toBe(1);

  const ifAst = funcAst.body.body[0] as IfStatement;
  expect(ifAst.type).toBe('IfStatement');
  expect(ifAst.alternate.type).toBe('BlockStatement');

  const elseAst = ifAst.alternate as BlockStatement;
  expect(elseAst.body.length).toBe(1);

  const expElseAst = elseAst.body[0] as ExpressionStatement;
  expect(expElseAst.type).toBe('ExpressionStatement');
  expect(expElseAst.expression).not.toBeUndefined();

  const callExpr = expElseAst.expression as CallExpression;
  expect(callExpr.type).toBe('CallExpression');
  expect(callExpr.callee.type).toBe('MemberExpression');
  expect((callExpr.callee as MemberExpression).object.type).toBe('Identifier');
  expect((callExpr.callee as MemberExpression).property.type).toBe('Identifier');
  expect(callExpr.arguments.length).toBe(1);

  const {object, property} = (callExpr.callee as MemberExpression);
  expect((object as Identifier).name).toBe('console');
  expect((property as Identifier).name).toBe('log');

  const arg = callExpr.arguments[0] as StringLiteral;
  expect(arg.type).toBe('StringLiteral');
  expect(arg.value).toBe(`"a" is not a string`)
});

test('import file as string data', () => {
  jest.mock('./templates/template.import-data.js?raw', () => 'const myVar = 10;', {
    virtual: true,
  });
  const result = require('./templates/template.import-data.js?raw');
  console.log(result);
  expect(result?.trim()).toBe('const myVar = 10;');
})
