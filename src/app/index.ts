import {importFile} from './lib/tokens/import-file';
import * as ts from 'typescript';

const chalk = require('chalk');
const path = require('path');

// if (!supportsColor) {
//   const ctx = new Instance({level: 3});
//   console.log(ctx.yellowBright('Hello world!'));
// }
//
// console.log('\x1b[36m%s\x1b[0m', 'I am cyan');
// console.log(red('user'));
// console.log(supportsColor);


export class MyGenerator {
  getMessage(userName) {
    return `Hello, ${chalk.red(userName)}!`;
  }

  path() {
    return importFile('');
  }

  code() {
    // return require(`./../../src/app/${this.getPath('templ.js')}?raw`);
  }
  private getPath(name) {
    return `${name}`;
  }
}

const source = "let x: string  = 'string'";

let result = ts.transpileModule(source, { compilerOptions: {
  module: ts.ModuleKind.CommonJS }});

console.log(JSON.stringify(result));

console.log(`Hello, ${chalk.red('word')}!`);
