import {
  ArrayRender,
  BooleanRender,
  NullRender,
  NumberRender,
  ObjectRender,
  RegexRender,
  StringRender,
  UndefinedRender
} from './token-render';
import {
  TokenArray,
  TokenBoolean,
  TokenNull,
  TokenNumber,
  TokenObject,
  TokenRegex,
  TokenString,
  TokenUndefined
} from '../tokens/tokens';
import {RenderDependencyMock} from './token-render.tests.mocks';

// Null
test('Render null token', () => {
  const result = new NullRender().render(new TokenNull());
  expect(result).toBe('null');
});

// Undefined
test('Render undefined token', () => {
  const result = new UndefinedRender().render(new TokenUndefined());
  expect(result).toBe('undefined');
});

// String
test('Render string token with regular value', () => {
  const result = new StringRender().render(new TokenString('regular'));
  expect(result).toBe(`'regular'`);
});

test('Render string token with empty value', () => {
  const result = new StringRender().render(new TokenString(undefined));
  expect(result).toBe(`''`);
})

// Number
test('Render number token with regular value', () => {
  const positive = new NumberRender().render(new TokenNumber(10));
  const negative = new NumberRender().render(new TokenNumber(-10));
  expect(positive).toBe('10');
  expect(negative).toBe('-10');
});

test('Render number token with NaN value', () => {
  const result = new NumberRender().render(new TokenNumber(NaN));
  expect(result).toBe('NaN');
});

test('Render number token with Infinity value', () => {
  const positive = new NumberRender().render(new TokenNumber(Infinity));
  const negative = new NumberRender().render(new TokenNumber(-Infinity));
  expect(positive).toBe('Infinity');
  expect(negative).toBe('-Infinity');
});

// Boolean
test('Render boolean token with regular value', () => {
  const positive = new BooleanRender().render(new TokenBoolean(true));
  const negative  = new BooleanRender().render(new TokenBoolean(false));
  expect(positive).toBe('true');
  expect(negative).toBe('false');
});

// Regex
test('Render regex token with regular value', () => {
  const result = new RegexRender().render(new TokenRegex(/([A-Z])\w+/));
  expect(result).toBe(`/([A-Z])\\w+/`);
});


// Object
test('Render object token with 2 regular fields', () => {
  const tokenObject = new TokenObject({
    user: new TokenString('value'),
    age: new TokenNumber(10)
  });
  const mock = RenderDependencyMock.createForObject();
  const objectRender = new ObjectRender(mock);
  const result = objectRender.render(tokenObject);

  expect(result).toBe(`{user:'value',age:10}`);
});

test('Render object token with object field', () => {
  const tokenObject = new TokenObject({
    users: new TokenObject({
      count: new TokenNumber(10)
    })
  });
  const mock = RenderDependencyMock.createForObject();
  const objectRender = new ObjectRender(mock);
  mock.append('object', objectRender);
  const result = objectRender.render(tokenObject);

  expect(result).toBe('{users:{count:10}}');
});

// Array
test('Render array token with 2 regular fields', () => {
  const tokenArray = new TokenArray([
    new TokenNumber(10),
    new TokenString('value')
  ]);
  const mock = RenderDependencyMock.createForObject();
  const arrayRender = new ArrayRender(mock);
  const result = arrayRender.render(tokenArray);

  expect(result).toBe(`[10,'value']`);
});

test('Render array token with nested array', () => {
  const tokenArray = new TokenArray([
    new TokenNumber(10),
    new TokenArray([
      new TokenNumber(10)
    ])
  ]);
  const mock = RenderDependencyMock.createForObject();
  const arrayRender = new ArrayRender(mock);
  mock.append('array', arrayRender);
  const result = arrayRender.render(tokenArray);

  expect(result).toBe('[10,[10]]');
});
