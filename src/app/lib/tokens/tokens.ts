import {TokenInvokedType, TokenType, TokenVariableScope} from './token-types';


export interface TokenBase {
  type: TokenType;
}

// TokenEmpty

export class TokenNull implements TokenBase {
  type: TokenType = 'null';
}

export class TokenUndefined implements TokenBase {
  type: TokenType = 'undefined';
}

// TokenSimple

export class TokenString implements TokenBase {
  type: TokenType = 'string';
  constructor(public value: string) {
  }
}

export class TokenNumber implements TokenBase {
  type: TokenType = 'number';
  constructor(public value: number) {
  }
}

export class TokenBoolean implements TokenBase {
  type: TokenType = 'boolean';
  constructor(public value: boolean) {
  }
}

export class TokenRegex implements TokenBase {
  type: TokenType = 'regex';
  constructor(public value: RegExp) {
  }
}

// TokenObject

export interface TokenObjectField {
  [name: string]: TokenBase;
}

export class TokenObject implements TokenBase {
  type: TokenType = 'object';
  constructor(public fields: TokenObjectField) {
  }
}

export class TokenArray implements TokenBase {
  type: TokenType = 'array';
  constructor(public items: TokenBase[]) {
  }
}

// TokenExec

export class TokenObjectCtor implements TokenBase {
  type: TokenType = 'objectCtor';
  import?: TokenImport;
  constructor(public args: TokenBase[], public namespace: string | null = null) {
  }
}

export class TokenFunctionExec implements TokenBase {
  type: TokenType = 'functionExec';
  import?: TokenImport;
  constructor(public args: TokenBase[], public namespace: string | null = null) {
  }
}

// TokenCode
export class TokenFunction implements TokenBase {
  type: TokenType = 'function';
  constructor(public args: string[], public body: string) {
  }
}

export class TokenArrowFunction implements TokenBase {
  type: TokenType = 'arrowFunction';
  constructor(public args: string[], public body: string) {
  }
}

export class TokenVariable implements TokenBase {
  type: TokenType = 'variable';
  constructor(public name: string) {
  }
}

export class TokenGlobalVariable implements TokenBase {
  type: TokenType = 'global-variable';
  constructor(public name: string, public imports: TokenImport) {
  }
}

// TokenInvokedType

export interface TokenInvokedBase {
  type: TokenInvokedType;
}

export class TokenDeclare implements TokenInvokedBase {
  type: TokenInvokedType = 'declare';
  constructor(public variable: TokenVariable, public scope: TokenVariableScope) {
  }
}

export class TokenAssign implements TokenInvokedBase {
  type: TokenInvokedType = 'assign';
  constructor(public variable: TokenDeclare | TokenVariable | TokenGlobalVariable, public body: TokenBase) {
  }
}

export class TokenImport implements TokenInvokedBase {
  type: TokenInvokedType = 'import';
  constructor(public name: string | string[], public path: string) {
  }
}
