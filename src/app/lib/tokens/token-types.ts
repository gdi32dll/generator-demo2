export type TokenSimpleType = 'string' | 'number' | 'boolean' | 'regex';
export type TokenEmptyType = 'undefined' | 'null';
export type TokenObjectType = 'object' | 'array';
export type TokenExecType = 'objectCtor' | 'functionExec';
export type TokenCodeType = 'function' | 'arrowFunction' | 'variable' | 'global-variable';

export type TokenPrimitiveType = TokenSimpleType | TokenEmptyType;
export type TokenValueType = TokenPrimitiveType | TokenObjectType | TokenExecType;
export type TokenType = TokenValueType | TokenCodeType;

export type TokenInvokedType = 'declare' | 'assign' | 'import';

export type TokenVariableScope = 'global' | 'local' | 'const' | 'declared';
