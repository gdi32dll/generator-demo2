import {
  TokenArray,
  TokenBase,
  TokenBoolean,
  TokenInvokedBase,
  TokenNull,
  TokenNumber, TokenObject, TokenRegex,
  TokenString,
  TokenUndefined
} from '../tokens/tokens';
import {RenderDependency} from './render-infrastructure';

export abstract class BaseRender<T extends (TokenBase | TokenInvokedBase)> {
  abstract render(token: T): string;
}

// Render empty

export class NullRender extends BaseRender<TokenNull> {
  render(token: TokenNull) {
    return 'null';
  }
}

export class UndefinedRender extends BaseRender<TokenUndefined> {
  render(token: TokenUndefined) {
    return 'undefined';
  }
}

// Render simple

export class StringRender extends BaseRender<TokenString> {
  render(token: TokenString): string {
    return token.value ? `'${token.value}'` : `''`;
  }
}

export class NumberRender extends BaseRender<TokenNumber> {
  render(token: TokenNumber): string {
    if (Number.isNaN(token.value)) {
      return 'NaN';
    }
    return `${token.value}`;
  }
}

export class BooleanRender extends BaseRender<TokenBoolean> {
  render(token: TokenBoolean): string {
    return `${token.value}`;
  }
}

export class RegexRender extends BaseRender<TokenRegex> {
  render(token: TokenRegex): string {
    return `/${token.value.source}/`;
  }
}

// Render object

export class ObjectRender extends BaseRender<TokenObject> {

  constructor(private dependency: RenderDependency) {
    super();
  }

  render(token: TokenObject): string {
    const fields = [];
    for (const fieldTokenName in token.fields) {
      if (token.fields.hasOwnProperty(fieldTokenName)) {
        const fieldToken = token.fields[fieldTokenName];
        const render = this.dependency.getRender(fieldToken.type);
        const value = render.render(fieldToken);
        fields.push({fieldTokenName, value});
      }
    }
    const output = fields.map(x => `${x.fieldTokenName}:${x.value}`).join(',');
    return `{${output}}`;
  }
}

export class ArrayRender extends BaseRender<TokenArray> {

  constructor(private dependency: RenderDependency) {
    super();
  }

  render(token: TokenArray): string {
    const items = [];
    for (const tokenItem of token.items) {
      const render = this.dependency.getRender(tokenItem.type);
      items.push(render.render(tokenItem));
    }
    const output = items.join(',');
    return `[${output}]`;
  }

}
