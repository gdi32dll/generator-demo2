// const index = require('./index');
import {MyGenerator} from './index';

const {red} = require('chalk');

test('test index.ts', () => {
  const generator = new MyGenerator();
  expect(generator.getMessage('user')).toBe(`Hello, ${red('user')}!`);
});
