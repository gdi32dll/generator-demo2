import {TokenType} from '../tokens/token-types';
import {BaseRender} from './token-render';
import {TokenBase, TokenInvokedBase} from '../tokens/tokens';

export interface RenderDependency {
  getRender(tokenName: TokenType): BaseRender<(TokenBase | TokenInvokedBase)>;
}
